# Summit-ADF

http://www.oracle.com/technetwork/developer-tools/jdev/learnmore/index-098948.html  
http://docs.oracle.com/middleware/1221/adf/develop/GUID-17129734-FA96-4637-998B-BE8ABC1EAD9D.htm#ADFFD121  


    # yum install -y git

    # su - oracle12

    $ cd /tmp/
    $ git clone https://github.com/oracle-adf/Summit-ADF

    $ cd /tmp/Summit-ADF/SummitADF_Schema1221/Database/scripts


    $ sqlplus system/<password>

    SQL> spool result.txt
    SQL> @build_summit_schema.sql;
    SQL> spool off;
    SQL> exit


This script creates user: summit_adf
